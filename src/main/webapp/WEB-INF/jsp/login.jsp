<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"	
        integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M"	crossorigin="anonymous">
    <title>Emids Quote Generator</title>
</head>


<body>
    <font color="red">${errorMessage}</font>
    <form method="post">
        Username : <input type="text" class="form-control" name="username" />
        Password : <input type="password" class="form-control" name="password" /> 
        <button class="btn btn-lg btn-primary btn-block" type="submit">Login </button>
    </form>
</body>

</html>