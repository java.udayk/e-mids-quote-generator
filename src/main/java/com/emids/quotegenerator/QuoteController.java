package com.emids.quotegenerator;

import java.text.NumberFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class QuoteController {
	private final Logger logger = LoggerFactory.getLogger(QuoteController.class);

	@RequestMapping(value = "/calculate", method = RequestMethod.POST)
	public ModelAndView calculate(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("quote");

		String gender = request.getParameter("gender");
		String age = request.getParameter("age");

		String hypertension = request.getParameter("hypertension") == null ? "false" : "true";
		String bloodpressure = request.getParameter("bloodpressure") == null ? "false" : "true";
		String bloodsugar = request.getParameter("bloodsugar") == null ? "false" : "true";
		String overweight = request.getParameter("overweight") == null ? "false" : "true";

		String smoking = request.getParameter("smoking") == null ? "false" : "true";
		String alcohol = request.getParameter("alcohol") == null ? "false" : "true";
		String dailyexercise = request.getParameter("dailyexercise") == null ? "false" : "true";
		String drugs = request.getParameter("drugs") == null ? "false" : "true";

		double basePremium = 5000;
		String mr = "";
		final double fixedbasePremium = 5000;// fixed

		double revisedBasePremium = calculateRevisedPremiumForAge(basePremium, fixedbasePremium, age);
		
		if (gender.equals("male")) {
			mr = "Mr.";
			revisedBasePremium += (0.02 * revisedBasePremium);// Gender rule: Male vs female vs Other % -> Increase 2% over standard slab for Males
		}
		//Pre-existing conditions (Hypertension | Blook pressure | Blood sugar | Overweight) -> Increase of 1% per condition
		if (hypertension.equals("true")) {
			revisedBasePremium += (0.01 * revisedBasePremium);// 1% +
		}
		if (bloodsugar.equals("true")) {
			revisedBasePremium += (0.01 * revisedBasePremium);// 1% +
		}
		if (bloodpressure.equals("true")) {
			revisedBasePremium += (0.01 * revisedBasePremium);// 1% +
		}
		if (overweight.equals("true")) {
			revisedBasePremium += (0.01 * revisedBasePremium);// 1% +
		}
		//Good habits (Daily exercise) -> Reduce 3% for every good habit
		if (dailyexercise.equals("true")) {
			revisedBasePremium -= (0.03 * revisedBasePremium); // 3% -
		}
		//Bad habits (Smoking | Consumption of alcohol | Drugs) -> Increase 3% for every bad habit
		if (smoking.equals("true")) {
			revisedBasePremium += (0.03 * revisedBasePremium);// 3% +
		}
		if (alcohol.equals("true")) {
			revisedBasePremium += (0.03 * revisedBasePremium);// 3% +
		}
		if (drugs.equals("true")) {
			revisedBasePremium += (0.03 * revisedBasePremium);// 3% +
		}

		NumberFormat nf = NumberFormat.getInstance();
		mav.addObject("message", "Health Insurance Premium for " + mr + "" + request.getParameter("userName") + ": Rs."
				+ nf.format(revisedBasePremium));
		return mav;
	}

	
	
	double calculateRevisedPremiumForAge(double revisedBasePremium, double fixedbasePremium, String age) {
		
		
		if (Integer.parseInt(age) < 18) {
			revisedBasePremium  = fixedbasePremium;   //Base premium for anyone below the age of 18 years = Rs. 5,000
		}else if (Integer.parseInt(age) >= 18 && Integer.parseInt(age) <= 25) {
			revisedBasePremium += (0.1 * fixedbasePremium);// % increase based on age: 18-25 -> + 10% 
		}else if (Integer.parseInt(age) >= 25 && Integer.parseInt(age) <= 30) {
			revisedBasePremium += (0.2 * fixedbasePremium);// % increase based on age:  25-30 -> +10% 
		}else if (Integer.parseInt(age) >= 30 && Integer.parseInt(age) <= 35) {
			revisedBasePremium += (0.3 * fixedbasePremium);// % increase based on age: 30-35 -> +10% 
		}else if (Integer.parseInt(age) >= 35 && Integer.parseInt(age) <= 40){
			revisedBasePremium += (0.4 * fixedbasePremium);//% increase based on age:  35-40 -> +10% 
		}else {
			revisedBasePremium = (0.4 * fixedbasePremium); // 40% increase for reaching 40
			//40+ -> 20% increase every 5 years
			int factor = (Math.round((Integer.parseInt(age) - 40)/5));
			revisedBasePremium += factor * (0.2) * revisedBasePremium;
		}
		
		return revisedBasePremium;
	}



	@RequestMapping(value = "/quote", method = RequestMethod.GET)
	public ModelAndView showRegister(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("quote");
	
		
		return mav;
	}


}
