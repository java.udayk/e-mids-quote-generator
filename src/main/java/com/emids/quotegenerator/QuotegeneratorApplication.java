package com.emids.quotegenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.emids.quotegenerator")
public class QuotegeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuotegeneratorApplication.class, args);
	
	}
	
	
	
}
