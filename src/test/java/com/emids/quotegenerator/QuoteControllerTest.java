package com.emids.quotegenerator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(value=QuoteControllerTest.class, secure = false)
public class QuoteControllerTest {
	
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	QuoteController testQuoteController;
	
	@Test
	public void calculateRevisedBasePremiumTestForAgeLessThan18() {
		
		double basePremium = 5000.00;
		double fixedbasePremium = 5000.00;
		double revisedBasePremium = 0.00;
		String age = "16";
				
		revisedBasePremium = testQuoteController.calculateRevisedPremiumForAge(basePremium, fixedbasePremium, age);		
		
		assertEquals(0,revisedBasePremium , 5000.00);		
	}
	
	@Test
	public void calculateRevisedBasePremiumTestForMale() {
		
		double basePremium = 5000.00;
		double fixedbasePremium = 5000.00;
		double revisedBasePremium = 0.00;
		String age = "16";
		String gender = "male";
				
		revisedBasePremium = testQuoteController.calculateRevisedPremiumForAge(basePremium, fixedbasePremium, age);		
		if (gender.equals("male")) {
		    revisedBasePremium += (0.02 * revisedBasePremium);// Gender rule: Male vs female vs Other % -> Increase 2% over standard slab for Males		
		}
		assertEquals(0,revisedBasePremium , 5100.00);		
	}
	
	@Test
	public void calculateRevisedBasePremiumTestForHealthRiskTrue() {
		
		double basePremium = 5000.00;
		double fixedbasePremium = 5000.00;
		double revisedBasePremium = 0.00;
		String age = "16";
		String gender = "male";
		String hypertension = "true";		
		String bloodpressure = "true";		
		String bloodsugar =  "true";		
		String overweight = "true";		
		String smoking = "true";		
		String alcohol = "true";		
		String dailyexercise = "true";		
		String drugs = "true";
				
		revisedBasePremium = testQuoteController.calculateRevisedPremiumForAge(basePremium, fixedbasePremium, age);		
		if (gender.equals("male")) {
		    revisedBasePremium += (0.02 * revisedBasePremium);// Gender rule: Male vs female vs Other % -> Increase 2% over standard slab for Males		
		}
		if (hypertension.equals("true")) {			
		    revisedBasePremium += (0.01 * revisedBasePremium);// 1% +		
		}
		if (bloodsugar.equals("true")) {			
		    revisedBasePremium += (0.01 * revisedBasePremium);// 1% +		
		}		
		if (bloodpressure.equals("true")) {			
		    revisedBasePremium += (0.01 * revisedBasePremium);// 1% +		
		}	
		if (overweight.equals("true")) {			
		    revisedBasePremium += (0.01 * revisedBasePremium);// 1% +		
		}
		
		assertEquals(0,revisedBasePremium , 5406.00);		
	}

}
